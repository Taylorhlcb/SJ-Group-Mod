#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Description
This module is dedicated to authenticating Telegram users to prove they are a person when they join a chatroom.
The purpose of the bot is to keep API bots and user bots out of the chatroom.



"""

# https://core.telegram.org/bots/api

# Built-in imports
import urllib
import json
import sqlite3
import re as regex
import os
import sys
import string
from time import time, sleep
from random import choice
from pprint import pprint

# Libs
import requests

# Own modules
import config_manager

__author__ = 'Taylor Halcomb'
__copyright__ = 'Copyright 2019, auth_bot'
__credits__ = ['SpookCity']
__license__ = 'GPL General Public License'
__version__ = '2.4.1'
__maintainer__ = 'Taylor Halcomb'
__email__ = 'taylortextalert@gmail.com'
__status__ = 'Beta'


# Generate new captcha with a default length of 4 digits
def captcha_generator(length=4):
    alphabet_numbers = list("abcdefghijklmnopqrstuvwxyz0123456789")
    captcha = ''
    for select in range(length):
        captcha = captcha + choice(alphabet_numbers)
    return(captcha)

class authUser:
    def __init__(self):
        ### Config settings ###
        # Bot info settings #
        self.config_manager_obj = config_manager.config()
        # Start logger #
        self.logger = self.config_manager_obj.logger
        self.logger.info("Starting auth_bot with {} config.".format(self.config_manager_obj.config_name))
        self.cfg = self.config_manager_obj.config_parser
        self.bot_token = self.cfg.get('bot_info_settings', 'bot_token')
        self.chat_id = self.cfg.getint('bot_info_settings', 'chat_id')
        try:
            self.admin_chat_id = self.cfg.getint('bot_info_settings', 'admin_chat_id')
        except:
            self.admin_chat_id = None
        self.bot_name = self.cfg.get('bot_info_settings', 'bot_name')
        # Bot api settings #
        self.update_string = "getUpdates?timeout={}&offset={}&limit={}".format(
                self.cfg.getint('bot_api_settings', 'pipe_timeout'),
                self.cfg.getint('bot_api_settings', 'pipe_offset'),
                self.cfg.getint('bot_api_settings', 'pipe_limit'))
        # bot chat group settings #
        self.group_name = self.cfg.get('bot_chat_group_settings', 'group_name')
        self.group_rating = self.cfg.get('bot_chat_group_settings', 'group_rating')
        self.welcome_message = self.cfg.get('bot_chat_group_settings', 'welcome_message', fallback = "") + " "
        self.no_bots_allowed = self.cfg.getboolean('bot_chat_group_settings', 'no_bots_allowed')
        # bot auth settings #
        self.auth_message = self.cfg.get('bot_auth_settings', 'auth_message', fallback = "")
        self.auth_complete_message = self.cfg.get('bot_auth_settings', 'auth_complete_message')
        self.auth_complete_media = self.cfg.get('bot_auth_settings', 'auth_complete_media', fallback = None)
        self.admin_auth_complete_message = self.cfg.get('bot_auth_settings', 'admin_auth_complete_message')
        self.user_response_timeout = self.cfg.getint('bot_auth_settings', 'user_response_timeout')
        self.admin_kick_message = self.cfg.get('bot_auth_settings', 'admin_kick_message')
        self.admin_join_message = self.cfg.get('bot_auth_settings', 'admin_join_message')
        if self.admin_chat_id:
            self.alert_join = self.cfg.getboolean('bot_auth_settings', 'alert_join')
            self.alert_auth = self.cfg.getboolean('bot_auth_settings', 'alert_auth')
            self.alert_kick = self.cfg.getboolean('bot_auth_settings', 'alert_kick')
        else:
            self.alert_join = False
            self.alert_auth = False
            self.alert_kick = False
        ### END of Config settings ###
        self.dbname = "storage.sqlite"
        self.conn = sqlite3.connect(self.config_manager_obj.config_manage_path + os.sep + self.dbname)
        self.url = "https://api.telegram.org/bot{}/".format(self.bot_token)
        self.update_string = self.url + self.update_string
        self.authlist_id = dict()
        self.authlist_username = dict()
        self.timeout_dict = dict()
        self.management = list()
        self.echo_cooldown_dict = dict()
        # https://api.telegram.org/bot#####/getUpdates?timeout=10&offset=-50&limit=50

    # Simply fetch the json update list
    # https://core.telegram.org/bots/api#getupdates
    def get_Updates_return_json(self):
        while True:
            response = self.request_url(self.update_string)
            json_response_data = json.loads(response.content)# Use json lib return parsed response
            if "result" in json_response_data:
                return json_response_data["result"]
            print("Error when gathering results from json object")
            print(response)
            sleep(1)

    def get_Admins_ruturn_list(self, data_type):
        assert(data_type != 'member_username' or 'id')
        response = self.request_url(self.url + "getChatAdministrators?chat_id={}".format(self.chat_id))
        json_response_data = json.loads(response.content)# Use json lib return parsed response
        if "result" in json_response_data:
            admin_list = []
            for index, user in enumerate(json_response_data["result"]):
                if user['user']['is_bot'] == False:
                    if data_type == 'id':
                        admin_list.append(user['user']['id'])
                    elif data_type == 'member_username':
                        admin_mention = "[{}](tg://user?id={})".format(self.take_message_return_username(user['user']),user['user']['id'])
                        admin_list.append(admin_mention)
            return admin_list
        else:
            return ['Error: No admins found in results']

    def request_url(self, url):
        start_time = time()
        while True:
            try:
                #print("Request URL")
                #start_time = time()
                #print(url)
                response = requests.get(url)
                #print("Response = {} Time = {}".format(response, time() - start_time))
                response.raise_for_status()
                if response.json():
                    return response
                else:
                    print("Request returned a response that is not JSON.")
            except requests.exceptions.HTTPError as errHTTP:
                print("Http Error: ",errHTTP)
            except requests.exceptions.ConnectionError as errConnection:
                print("Error Connecting: ",errConnection)
            except requests.exceptions.Timeout as errTimeout:
                print("Timeout Error: ",errTimeout)
            except requests.exceptions.RequestException as err:
                print("OOps: Something Else ",err)
            print("Retrying in 10 seconds... (Exception for {} seconds)".format(int(time()) - int(start_time)))
            sleep(10)
            print("Retrying now")

    def request_url_once(self,url):
        try:
            response = requests.get(url)
            response.raise_for_status()
            if response.json():
                return response
            else:
                print("Request returned a response that is not JSON.")
                return None
        except requests.exceptions.HTTPError as errHTTP:
            print("Http Error: ",errHTTP)
            return(errHTTP)
        except requests.exceptions.ConnectionError as errConnection:
            print("Error Connecting: ",errConnection)
            return(errConnection)
        except requests.exceptions.Timeout as errTimeout:
            print("Timeout Error: ",errTimeout)
            return(errTimeout)
        except requests.exceptions.RequestException as err:
            print("OOps: Something Else ",err)
            return(err)

    # https://core.telegram.org/bots/api#sendmessage
    def send_plain_text(self, chat_id, plain_text, markup = '', disable_web_page_preview = ''):
        address = self.url + "sendMessage?chat_id={}&text={}{}{}".format(chat_id, urllib.parse.quote_plus(plain_text), markup, disable_web_page_preview)
        print("Request send_plain_text in chat_id '{}', with text '{}'".format(chat_id, plain_text))
        start_time = time()
        response = self.request_url(address)
        print("Response = {} Time = {}".format(response, time() - start_time))

    # https://core.telegram.org/bots/api#senddocument
    def send_media_message(self, chat_id, document_id):
        address = self.url + "sendDocument?chat_id={}&document={}".format(chat_id, document_id)
        print("Request send_media_message in chat_id '{},' with document_id '{}'".format(chat_id, document_id))
        start_time = time()
        response = self.request_url(address)
        print("Response = {} Time = {}".format(response, time() - start_time))

    # https://core.telegram.org/bots/api#deletemessage
    def delete_message(self, chat_id, message_id):
        address = self.url + "deleteMessage?chat_id={}&message_id={}".format(self.chat_id, message_id)
        print("Request delete_message in chat_id '{},' with message_id '{}'".format(chat_id, message_id))
        start_time = time()
        response = self.request_url_once(address)
        print("Response = {} Time = {}".format(response, time() - start_time))

    # https://core.telegram.org/bots/api#kickchatmember
    def kick_user(self, chat_id, user_id):
        address = self.url + "kickChatMember?chat_id={}&user_id={}".format(chat_id, user_id)
        print("Request kick_user in chat_id '{}', with user_id '{}'".format(chat_id, user_id))
        start_time = time()
        response = self.request_url_once(address)
        print("Response = {} Time = {}".format(response, time() - start_time))

    # https://core.telegram.org/bots/api#restrictchatmember
    def unban_user(self, chat_id, user_id):
        address = self.url + "unbanChatMember?chat_id={}&user_id={}".format(chat_id, user_id)
        print("Request unban_user in chat_id '{}', with user_id '{}'".format(chat_id, user_id))
        start_time = time()
        response = self.request_url_once(address)
        print("Response = {} Time = {}".format(response, time() - start_time))

    def restrict_user(self, chat_id, user_id, 
                        can_send_messages = "false",
                        can_send_media_messages = "false",
                        can_send_other_messages = "false",
                        can_add_web_page_previews = "false"):
        address = self.url + ("restrictChatMember?chat_id={}&user_id={}" +
                                        "&can_send_messages={}" +
                                        "&can_send_media_messages={}" +
                                        "&can_send_other_messages={}" +
                                        "&can_add_web_page_previews={}").format(
                                                chat_id, user_id,
                                                can_send_messages, can_send_media_messages,
                                                can_send_other_messages, can_add_web_page_previews)
        print("Request restrict_user in chat_id '{}', with user_id '{}'".format(chat_id, user_id))
        start_time = time()
        response = self.request_url_once(address)
        print("Response = {} Time = {}".format(response, time() - start_time))

    # Make a request and return content and status code
    def take_message_return_username(self, user):
        member_username = "###"
        try:
            member_username = user["first_name"] + " " + user["last_name"]
        except:
            member_username = user["first_name"]
        return member_username

    # Check each message to see if it's a new member message
    def new_member_check(self, message, key_type):
        message = message[key_type]
        member_id = message["new_chat_member"]["id"]
        member_is_bot = message["new_chat_member"]["is_bot"]
        member_username = self.take_message_return_username(
                user = message["new_chat_member"])
        return member_id, member_is_bot, member_username
    
    def remove_bot_on_join(self, member_id, member_is_bot, member_username):
        # Automatically kick any new bot that joins
        if self.no_bots_allowed and member_is_bot:
            self.kick_user(chat_id = self.chat_id, user_id = member_id)
            if self.alert_kick:
                self.send_plain_text(
                        chat_id = self.admin_chat_id,
                        plain_text = self.admin_kick_message.format(member_username, "Being a Bot."))

    def send_auth_request(self, member_id, member_username):
        # Ask new user to auth if person has not authed before
        self.restrict_user(self.chat_id, member_id, 
                can_send_messages = "true",
                can_send_media_messages = "false",
                can_send_other_messages = "false",
                can_add_web_page_previews = "false")
        # Combine username with inline mention by ID
        mention = "[{}](tg://user?id={})".format(member_username, member_id)
        # Forward the new join message to mods
        if self.alert_join:
            self.send_plain_text(
                    chat_id = self.admin_chat_id,
                    plain_text = self.admin_join_message.format(mention, self.group_name),
                    markup = "&parse_mode=Markdown")
        auth_code = captcha_generator()
        # Create a key pair with their id and captcha ex: {886357933: 'przd'}
        self.authlist_id[member_id] = auth_code
        # Create a key pair with their id and timestamp
        self.timeout_dict[member_id] = int(time())
        # Create a key pair with their id and username
        self.authlist_username[member_id] = member_username
        # https://api.telegram.org/bot####/sendMessage?chat_id=673599929&text=TestReply
        # Send welcome message and captcha
        self.send_plain_text(chat_id = self.chat_id, 
                plain_text = self.auth_message.format(
                        mention,
                        self.group_rating,
                        self.welcome_message,
                        self.user_response_timeout, 
                        auth_code),
                markup = "&parse_mode=Markdown")

    def new_member_authenticate(self, message, key_type):
        if self.authlist_id:
            try:
                message = message[key_type]
                message_id = message["message_id"]
                member_id = message["from"]["id"]
                member_username = self.take_message_return_username(message["from"])
            except:
                return
            if member_id in self.authlist_id:
                print("Auth member typing")
                try:
                    if 'new_chat_member' not in message:
                        assert message["text"].lower() == self.authlist_id[member_id]
                except:
                    print("FAIL 1")
                    self.delete_message(chat_id = self.chat_id, message_id = message_id)
                    print("Delete success")
                try:
                    if message["text"].lower() == self.authlist_id[member_id]:
                        self.to_table_to_key_insert_variable("users", "userID", member_id)
                        del self.authlist_id[member_id]
                        del self.timeout_dict[member_id]
                        del self.authlist_username[member_id]
                        self.restrict_user(chat_id = self.chat_id, user_id = member_id, 
                                can_send_messages = "true",
                                can_send_media_messages = "true",
                                can_send_other_messages = "true",
                                can_add_web_page_previews = "true")
                        # Send message confirming auth completion
                        self.send_plain_text(
                                chat_id = self.chat_id,
                                plain_text = self.auth_complete_message.format(member_username))
                        # Send message to mod chat confirming auth completion
                        if self.alert_auth:
                            self.send_plain_text(
                                    chat_id = self.admin_chat_id,
                                    plain_text = self.admin_auth_complete_message.format(member_username))
                        # Send a derpy gif if they succeed
                        if self.auth_complete_media:
                            self.send_media_message(
                                    chat_id = self.chat_id,
                                    document_id = self.auth_complete_media)
                except:
                    print("FAIL 2")
                    pass # Not a text message - No auth sent

    def auth_timeout_kick(self):
        if self.timeout_dict:
            for member_id in list(self.timeout_dict):
                # After user_response_timeout seconds, kick user
                if int(time()) - self.timeout_dict[member_id] >= self.user_response_timeout:
                    #print("Kicking user: '{}' Reason: Authentication timeout".format(member_id))
                    self.restrict_user(chat_id = self.chat_id, user_id = member_id, 
                            can_send_messages = "true",
                            can_send_media_messages = "true",
                            can_send_other_messages = "true",
                            can_add_web_page_previews = "true")
                    self.kick_user(chat_id = self.chat_id, user_id = member_id)
                    self.unban_user(chat_id = self.chat_id, user_id = member_id) # COMMENT LATER, THIS MAKES READDING SELVES EASY FOR TESTING 
                    if self.alert_kick:
                        self.send_plain_text(
                                chat_id = self.admin_chat_id,
                                plain_text = self.admin_kick_message.format(self.authlist_username[member_id], "Authentication timeout."))
                    del self.authlist_id[member_id]
                    del self.timeout_dict[member_id]
                    del self.authlist_username[member_id]

    def bot_command_check(self, message, key):
        try:
            message = message[key]
            message_entity = message['entities'][0]
            entity_type = message_entity['type']
            entity_length = message_entity['length']
            entity_offset = message_entity['offset']
            member_username = self.take_message_return_username(message["from"])
            member_id = message['from']['id']
            message_text = message['text']
            if entity_type == "bot_command" and \
                    entity_offset == 0:
                if entity_length == (len(self.bot_name) * 2) + 6 and \
                        message_text.startswith("/" + self.bot_name + "@" + self.bot_name + "_bot"):
                    return True
                if entity_length == (len(self.bot_name) + 1) and \
                        message_text.startswith('/' + self.bot_name):
                    return True
            else:
                return False
        except:
            return False

    def bot_command_parse(self, message, key):
        message = message[key]
        member_id = message['from']['id']
        message_text = message['text']
        name_length = len(self.bot_name)
        if message_text.startswith("/" + self.bot_name + "@" + self.bot_name + "_bot"):
            message_text = "/" + self.bot_name + message_text[(len(self.bot_name) * 2) + 6:]
        message_text = message_text[len(self.bot_name) + 2:]
        if " " in message_text:
            command = message_text.split(" ", 1)[0]
            params = message_text.split(" ", 1)[1]
        else:
            command = message_text
            print('empty command = ' + command)
            params = None
        return member_id, command, params

    def bot_command_list(self, member_id, command, params):
        print(command)
        if command == '':
            self.bot_echo_command(self.bot_name, text = self.cfg.get(self.bot_name, 'text'))
        if command is not None and self.cfg.has_option('bot_commands', command) and \
                self.bot_command_admin_required(member_id, command):
            command_type = self.cfg.get('bot_commands', command)
            if command_type == 'echo' and params == None:
                print("Echo used = " + command)
                self.bot_echo_command(command, text = self.cfg.get(command, 'text'))
            if command_type == 'builtin':
                if command == 'add_echo':
                    echo_name, echo_text = self.split_params(params)
                    self.bot_add_echo_command(echo_name = echo_name, echo_text = echo_text)
                elif command == 'remove_echo':
                    self.bot_remove_echo_command(echo_name = params)
                elif command == 'echo'and params == None:
                    self.bot_echo_command('echo', self.cfg.get(command, 'text'))
                elif command == 'echo_list':
                    self.bot_list_commands(params = 'echo')
                elif command == 'command_list':
                    self.bot_list_commands(params = 'builtin')
                elif command == 'version' and params == None:
                    self.bot_echo_command(command, text = "Current version: " + __version__)
                elif command == 'admins' and params == None:
                    self.bot_alert_admins(self.get_Admins_ruturn_list('member_username'))

    def bot_echo_command(self, command, text):
        if command in self.echo_cooldown_dict:
            current_cooldown = int(time()) - self.echo_cooldown_dict[command]
        else:
            current_cooldown = 999999999
        if current_cooldown >= self.cfg.getint(command, 'cooldown') and \
                text is not '':
            if command in self.echo_cooldown_dict:
                del self.echo_cooldown_dict[command]
            self.echo_cooldown_dict[command] = int(time())
            self.send_plain_text(
                    chat_id = self.chat_id,
                    plain_text = text)
        else:
            cooldown = (int(time()) - self.echo_cooldown_dict[command])
            self.send_plain_text(
                    chat_id = self.chat_id,
                    plain_text = 'Sorry, {} is still on cooldown ({}s/{}s)'.format(command, cooldown, self.cfg.getint(command, 'cooldown')))

    def bot_add_echo_command(self, echo_name = None, echo_text = None):
        if echo_name and echo_text:
            if not self.cfg.has_option('bot_commands', echo_name):
                try:
                    self.cfg.set('bot_commands', echo_name, 'echo')
                    self.cfg.add_section(echo_name)
                    self.cfg.set(echo_name, 'text', echo_text)
                    self.cfg.set(echo_name, 'cooldown', '600')
                    self.cfg.set(echo_name, 'admin_only', 'False')
                    self.cfg.set(echo_name, 'deletable', 'True')
                    self.config_manager_obj.write_config()
                    print("Completed adding echo")
                except:
                    try:
                        self.cfg.remove_option('bot_commands', echo_name)
                        self.cfg.remove_section(echo_name)
                        self.config_manager_obj.write_config()
                        self.send_plain_text(
                                chat_id = self.chat_id,
                                plain_text = ("Bad write to config. Please use normal text when writing an echo."))
                        print("Deleted bad values")
                    except:
                        print("Was not able to delete even")
                else:
                    self.send_plain_text(
                            chat_id = self.chat_id,
                            plain_text = ("Echo '{}' added to config.").format(echo_name))
            else:
                self.send_plain_text(
                        chat_id = self.chat_id,
                        plain_text = ("Echo '{}' already found in config.").format(echo_name))
        else:
            self.bot_echo_command('add_echo', text = self.cfg.get('add_echo', 'text'))

    def bot_remove_echo_command(self, echo_name = None):
        if echo_name:
            if self.cfg.has_option('bot_commands', echo_name):
                if self.cfg.getboolean(echo_name, 'deletable'):
                    self.cfg.remove_option('bot_commands', echo_name)
                    self.cfg.remove_section(echo_name)
                    self.config_manager_obj.write_config()
                    self.send_plain_text(
                            chat_id = self.chat_id,
                            plain_text = ("Echo '{}' removed from config.").format(echo_name))
                else:
                    self.send_plain_text(
                            chat_id = self.chat_id,
                            plain_text = ("Echo '{}' has deletable set to {} in config.").format(
                                    echo_name, 
                                    self.cfg.getboolean(echo_name, 'deletable')))
            else:
                self.send_plain_text(
                        chat_id = self.chat_id,
                        plain_text = ("No echo named '{}' in config.").format(echo_name))
        else:
            self.bot_echo_command('remove_echo', text = self.cfg.get('remove_echo', 'text'))

    def bot_list_commands(self, params):
        echo_list = []
        for pair in self.cfg.items('bot_commands'):
            bot_command = pair[0]
            command_type = pair[1]
            if command_type == params:
                command_text = self.cfg.get(bot_command, 'text')
                echo_list.append(bot_command + ' = ' + command_text + '\n\n')
        echo_list = ''.join(echo_list)
        if echo_list == '':
            echo_list = "No echo commands found."
        self.send_plain_text(
            chat_id = self.chat_id,
            plain_text = echo_list,
            markup = '&parse_mode=HTML',
            disable_web_page_preview = '&disable_web_page_preview=True')

    def bot_alert_admins(self, admin_list):
        admin_list = ' '.join(admin_list)
        self.send_plain_text(
            chat_id = self.chat_id,
            plain_text = (admin_list),
            markup = "&parse_mode=Markdown")

    def bot_command_admin_required(self, member_id, command):
        if self.cfg.getboolean(command, 'admin_only'):
            if member_id in self.get_Admins_ruturn_list('id'):
                return True
            else:
                self.send_plain_text(
                    chat_id = self.chat_id,
                    plain_text = "You do not have permission to use an admin_only command")
                return False
        else:
            return True

    def split_params(self, split):
        try:
            echo_name = str(split.split(" ", 1)[0])
            echo_text = str(split.split(" ", 1)[1])
        except: 
            echo_name = None
            echo_text = None
        return echo_name, echo_text

    def filter_Ascii(self, member_username):
        return list(filter(lambda x: x in set(string.printable), member_username))

    def cache_ids_on_startup(self):
        chat = self.get_Updates_return_json()
        for message in chat:
            #if message["update_id"] not in self.management: # Only check message if it has not been processed already
            self.management.append(message["update_id"]) # Take message id and store it in a list
        print("Cached {} messages on startup.".format(len(self.management)))

    def delete_real_bot (self, user_id, message_id, member_username):
        self.delete_message(chat_id = self.chat_id, message_id = message_id)

        self.kick_user(chat_id = self.chat_id, user_id = user_id)

        self.send_plain_text(
            chat_id = self.chat_id,
            plain_text = "ree")

        #self.send_plain_text(
        #    chat_id = self.admin_chat_id,
        #    plain_text = self.admin_kick_message.format(member_username, "Being a Bot."))

    # Main loop for checking messages
    def chat_management(self):
        # Fetch events from chat
        chat = self.get_Updates_return_json()
        id_list = []
        # Process messages
        for message in chat:
            # Keep track of current IDs
            id_list.append(message["update_id"])
            if message["update_id"] not in self.management:
                # Take message id and store it in a list
                self.management.append(message["update_id"])
                try:
                    key = list(message)[1]
                    message_chat_id = message[key]["chat"]["id"]
                except:
                    print("Fail with message: {}".format(message))
                else:
                    if self.chat_id == message_chat_id:
                        pprint(message)
                        print("Parsing message: '{}'".format(message["update_id"]))
                        if 'new_chat_member' in message[key]:
                            # Send a welcome message and auth code if a new member has joined
                            member_id, member_is_bot, member_username = self.new_member_check(message = message, key_type = key)
                            if member_is_bot:
                                self.remove_bot_on_join(member_id, member_is_bot, member_username)
                            elif member_id not in self.authlist_id and not self.fetch_user_info(member_id):
                                chars_valid = len(self.filter_Ascii(member_username))
                                chars_total = len(member_username)
                                if chars_total <= 3 and chars_valid < 1 or chars_total > 3 and chars_valid <= 3:
                                    self.delete_real_bot(member_id, message_chat_id, member_username)
                                else:
                                    self.send_auth_request(member_id, member_username)
                        try:
                            if self.bot_command_check(message, key):
                                print("Bot command True")
                                member_id, command, params = self.bot_command_parse(message, key)
                                self.bot_command_list(member_id, command, params)
                        except Exception as e:
                            print("Fatal error on bot command with :")
                            print(e)
                        # See if a user trying to authenticate
                        self.new_member_authenticate(message = message, key_type = key)
        # Make sure management list only contains current IDs
        self.management = id_list

    # Table to store successfully authed users
    def setup(self):
        cur = self.conn.cursor()
        stmt = "CREATE TABLE IF NOT EXISTS users (userID integer)"
        cur.execute(stmt)
        self.conn.commit()

    # Place successful users for longterm storage
    def to_table_to_key_insert_variable(self, table, key, variable):
        cur = self.conn.cursor()
        stmt = "INSERT INTO {}({}) VALUES(?)".format(table, key)
        args = (variable,)
        cur.execute(stmt, args)
        self.conn.commit()
        print("Commit to table '{}', key '{}', variable '{}'".format(table, key, variable))

    # Try to find user inside of the local db
    def fetch_user_info(self, userID):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM users WHERE userID=?", (int(userID), ))
        return(cur.fetchone())

def run(runclass):
    runclass.setup()
    runclass.cache_ids_on_startup()
    while True:
        runclass.chat_management()
        runclass.auth_timeout_kick()
        sleep(1)

def main():
    run(authUser())

if __name__ == '__main__':
    main()